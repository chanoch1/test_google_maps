import csv
import pprint
from time import sleep
from colorama import Fore, Style
from selenium import webdriver
from selenium.webdriver.common.by import By


class Google_maps:

    def __init__(self):
        with open('addres.csv', 'r') as f:
            data = csv.reader(f)
            self.address = list(data)
        self.array = [[0 for i in range(len(self.address) - 1)] for j in range(len(self.address))]
        self.go = "//*[@id='omnibox-directions']/div/div[2]/div/div/div[1]/div[4]/button/img"  # Foot
        self.travle = "//*[@id='omnibox-directions']/div/div[2]/div/div/div[1]/div[2]/button/img"  # Car

    def setup_method(self):  # open the browser
        self.driver = webdriver.Chrome(executable_path="chromedriver")

    def site(self):
        self.driver.get("https://www.google.co.in/maps/@10.8091781,78.2885026,7z")
        self.driver.set_window_size(1528, 816)

    def table_distance(self):  # Fills in the list
        self.searchplace(self.address[0][1])  # addres[row][0= name, 1= address]
        for i in range(len(self.address) - 1):
            if i > 0:
                self.destination(self.address[i][1])
            for j in range(i+1, len(self.address)):
                self.find(self.address[j][1])
                self.driver.implicitly_wait(10)
                dis, min = self.kilometers()
                dis = self.to_meters(dis)
                min = self.to_minut(min)
                self.array[i][j-1] = [dis, min]
                dis, min = self.opposite_direction()
                self.array[j][i] = [dis, min]
        pprint.pprint(self.array)

    # search locations
    def searchplace(self, to_place):
        self.driver.implicitly_wait(10)
        self.driver.find_element(By.CLASS_NAME, "tactile-searchbox-input").send_keys(to_place)
        self.driver.find_element(By.ID, "searchbox-searchbutton").click()
        self.directions()

    # get directions
    def directions(self):
        self.driver.implicitly_wait(10)
        self.driver.find_element(By.XPATH, "//*[@id='pane']/div/div[1]/div/div/div[4]/div[1]/button").click()

    def destination(self, from_place):
        des = self.driver.find_element(By.XPATH, " //*[@id='sb_ifc52']/input")
        des.click()
        des.send_keys(from_place)

    # find place
    def find(self, from_place):
        self.driver.implicitly_wait(10)
        place = self.driver.find_element(By.XPATH, "//*[@id='sb_ifc51']/input")
        place.click()
        place.send_keys(from_place)
        self.driver.implicitly_wait(10)
        self.driver.find_element(By.XPATH, self.travle).click()  # Car
        #self.driver.find_element(By.XPATH, self.go).click()  # Foot
        place.click()
        #self.driver.implicitly_wait(10)
        self.driver.find_element(By.XPATH, "//*[@id='directions-searchbox-0']/button[1]").click()

    def kilometers(self):
        self.driver.implicitly_wait(10)
        sleep(1)
        #Totalkilometers = self.driver.find_element(By.XPATH, "//*[@id='section-directions-trip-0']/div/div[3]/div[1]/div[2]")  # Foot
        Totalkilometers = self.driver.find_element(By.XPATH, "//*[@id='section-directions-trip-0']/div/div[1]/div[1]/div[2]/div")  # Car
        self.driver.implicitly_wait(10)
        #Totalminut = self.driver.find_element(By.XPATH, "//*[@id='section-directions-trip-0']/div/div[3]/div[1]/div[1]")  # Foot
        Totalminut = self.driver.find_element(By.XPATH, "//*[@id='section-directions-trip-0']/div/div[1]/div[1]/div[1]")  # Car
        return Totalkilometers.text, Totalminut.text

    def opposite_direction(self):
        self.driver.find_element(By.XPATH, "//*[@id='omnibox-directions']/div/div[3]/div[2]/button").click()  # Change direction
        self.driver.implicitly_wait(10)
        dis, min = self.kilometers()
        dis = self.to_meters(dis)
        min = self.to_minut(min)
        self.driver.find_element(By.XPATH, "//*[@id='omnibox-directions']/div/div[3]/div[2]/button").click()
        return dis, min

    def to_meters(self, txt_dis):
        num_txt = (txt_dis.split())
        num_txt[0] = float(num_txt[0])
        if num_txt[1] == "km":
            num_txt[0] = num_txt[0] * 1000
        return num_txt[0]

    def to_minut(self, txt_time):
        num_txt = (txt_time.split())
        if num_txt[1] == "hr":
            minuts = int(num_txt[0])*60
            if len(num_txt) == 4:
                minuts += int(num_txt[2])
        else:
            minuts = int(num_txt[0])
        return minuts

    def time_calculation(self, one_row_in_array_dis):
        big_time = one_row_in_array_dis[0][1]
        for i in range(1, len(one_row_in_array_dis)):
            if one_row_in_array_dis[i][1] > big_time:
                big_time = one_row_in_array_dis[i][1]
        return big_time

    def meters_kilometers(self, place_num):
        if place_num > 1000:
            place_num = str(place_num/1000) + " km"
        else:
            place_num = str(place_num) + " m"
        return place_num

    def minut_hours(self, time):
        if time < 60:
            return str(time) + " min"
        elif time % 60 == 0:
            return str(time/60) + " hr"
        else:
            return str(int(time/60)) + " hr " + str(time % 60) + " min"

    def sum_km(self, row):
        lskm = []
        for i in range(len(self.array) - 1):
            lskm.append(self.array[row][i][0])  # array[row][i= someone][0= km 1= time]
        return [self.meters_kilometers(sum(lskm)), sum(lskm)]

    def the_far(self, row):
        lstime = []
        for i in range(len(self.array) - 1):
            lstime.append(self.array[row][i][1])  # array[row][i= someone][0= km 1= time]
        who_long_time = [lstime.index(max(lstime)), max(lstime)]
        if who_long_time[0] >= row:
            who_long_time[0] += 1
        #  who_long_time[0= idx who long time][1= how long]
        return who_long_time

    def the_near(self, row):
        lstime = []
        for i in range(len(self.array) - 1):
            lstime.append(self.array[row][i][1])  # array[row][i= someone][0= km 1= time]
        who_little_time = [lstime.index(min(lstime)), min(lstime)]
        if who_little_time[0] >= row:
            who_little_time[0] += 1
        #  who_little_time[0= idx who little time][1= how long]
        return who_little_time

    def finaly(self, row):
        meeting = self.address[row][0]
        sum_km = self.sum_km(row)
        far = self.the_far(row)  # who_long_time[0= idx who long time][1= how long]
        near = self.the_near(row)  #  who_little_time[0= idx who little time][1= how long]
        print("The place is: " + Fore.RED + meeting + Style.RESET_ALL + " That the sum of all distances is: " + sum_km[0])
        print("The farthest place is: " + Fore.LIGHTBLUE_EX + self.address[far[0]][0] + Style.RESET_ALL + " That it takes " + self.minut_hours(far[1]) + "    And the closest place is: " + Fore.YELLOW + self.address[near[0]][0] + Style.RESET_ALL + " That it takes " + self.minut_hours(near[1]))

    def print_distance_km(self):
        lsdis = []
        for i in range(len(self.address)):
            sum_dis = self.sum_km(i)
            lsdis.append(sum_dis[1])
        return lsdis.index(min(lsdis))

    def the_far_goes_less(self):
        big = []
        for i in range(len(self.address)):
            big.append(self.time_calculation(self.array[i]))
        return big.index(min(big))

g = Google_maps()
g.setup_method()
g.site()
g.table_distance()

g.finaly(g.print_distance_km())
g.finaly(g.the_far_goes_less())
